class Fighter {
  constructor({ attack, defense, health, source }, side, critCombo) {
    this.atk = attack;
    this.critDamage = attack * 2;
    this.defense = defense;
    this.hp = health;
    this.isBlocking = false;
    this.initialHealth = health;
    this.healthBar = document.getElementById(`${side}-fighter-indicator`);
    this.healthBar.style.width = '100%';
    this.keyMap = new Map();
    this.lastCritTime = 0;
    this.isCritted = false;
  }

  get attack() {
    if (this.isBlocking) return 0;

    return this.atk;
  }

  set health(value) {
    if (this.isBlocking && !this.isCritted) return;

    const damagePercent = ((this.hp - value) / this.initialHealth) * 100;
    const newHealth = Math.max(parseInt(this.healthBar.style.width) - damagePercent, 0);
    this.healthBar.style.width = `${newHealth}%`;
    this.hp = Math.max(value, 0);
  }

  get health() {
    return this.hp;
  }

  get crit() {
    const now = new Date();

    if (now - this.lastCritTime < 10000) return 0;

    this.lastCritTime = now;
    return this.critDamage;
  }

  setBlock() {
    this.isBlocking = true;
  }

  liftBlock() {
    this.isBlocking = false;
  }
}

export default Fighter;
