import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if (fighter) {
    fighterElement.append(createFighterImage(fighter));
    fighterElement.append(
      createFighterStatBlock({
        ATK: fighter.attack,
        DEF: fighter.defense,
        HP: fighter.health,
      })
    );
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

function createFighterStatBlock(stats) {
  const STAT_COLORS = ['red', 'blue', 'green'];

  const statBlock = createElement({
    tagName: 'div',
    className: 'fighter-preview___statblock',
  });

  Object.entries(stats).forEach(([name, value], index) => {
    const stat = createElement({
      tagName: 'div',
      className: 'fighter-preview___stat',
    });
    const statName = createElement({
      tagName: 'span',
      className: `fighter-preview___statname ${STAT_COLORS[index]}`,
    });
    statName.innerHTML = name;
    const statValue = createElement({
      tagName: 'span',
      className: 'fighter-preview___statvalue',
    });
    statValue.innerHTML = value;

    stat.append(statName, statValue);
    statBlock.append(stat);
  });

  return statBlock;
}
