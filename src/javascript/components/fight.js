import { controls } from '../../constants/controls';
import Fighter from './fighter';
import getRandom from '../helpers/getRandom';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    const first = new Fighter(firstFighter, 'left', controls.PlayerOneCriticalHitCombination);
    const second = new Fighter(secondFighter, 'right', controls.PlayerTwoCriticalHitCombination);

    const keyMap = new Map();

    const handleKeydown = (e) => {
      if (e.repeat) return;

      switch (e.code) {
        case controls.PlayerOneAttack:
          second.health -= getDamage(first, second);
          break;
        case controls.PlayerOneBlock:
          first.setBlock();
          break;
        case controls.PlayerTwoAttack:
          first.health -= getDamage(second, first);
          break;
        case controls.PlayerTwoBlock:
          second.setBlock();
          break;
        case controls.PlayerOneCriticalHitCombination[0]:
        case controls.PlayerOneCriticalHitCombination[1]:
        case controls.PlayerOneCriticalHitCombination[2]:
          first.keyMap.set(e.code, true);

          if (controls.PlayerOneCriticalHitCombination.every((code) => first.keyMap.get(code))) {
            second.isCritted = true;
            second.health -= first.crit;
            first.isCritted = false;
          }
          break;
        case controls.PlayerTwoCriticalHitCombination[0]:
        case controls.PlayerTwoCriticalHitCombination[1]:
        case controls.PlayerTwoCriticalHitCombination[2]:
          second.keyMap.set(e.code, true);

          if (controls.PlayerTwoCriticalHitCombination.every((code) => second.keyMap.get(code))) {
            first.isCritted = true;
            first.health -= second.crit;
            first.isCritted = false;
          }
          break;
      }

      if (first.health <= 0 || second.health <= 0) {
        const winner = first.health === 0 ? secondFighter : firstFighter;
        document.removeEventListener('keydown', handleKeydown);
        document.removeEventListener('keyup', handleKeyup);
        resolve(winner);
      }
    };

    const handleKeyup = (e) => {
      switch (e.code) {
        case controls.PlayerOneBlock:
          first.liftBlock();
          break;
        case controls.PlayerTwoBlock:
          second.liftBlock();
          break;
        case controls.PlayerOneCriticalHitCombination[0]:
        case controls.PlayerOneCriticalHitCombination[1]:
        case controls.PlayerOneCriticalHitCombination[2]:
          first.keyMap.set(e.code, false);
        case controls.PlayerTwoCriticalHitCombination[0]:
        case controls.PlayerTwoCriticalHitCombination[1]:
        case controls.PlayerTwoCriticalHitCombination[2]:
          second.keyMap.set(e.code, false);
      }
    };

    document.addEventListener('keydown', handleKeydown);
    document.addEventListener('keyup', handleKeyup);
  });
}

export function getDamage(attacker, defender) {
  const hitPower = getHitPower(attacker);
  const blockPower = getBlockPower(defender);
  return Math.max(hitPower - blockPower, 0);
}

export function getHitPower(fighter) {
  const criticalHitChance = getRandom(1, 2);
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  const dodgeChance = getRandom(1, 2);
  return fighter.defense * dodgeChance;
}
