import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';

export function showWinnerModal(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const modalBody = createElement({ tagName: 'img', className: 'modal___image', attributes });
  modalBody.innerHTML = 'Close the modal to return to character selection';

  showModal({
    title: `${fighter.name.toUpperCase()} WINS`,
    bodyElement: modalBody,
    onClose: () => location.reload(),
  });
}
