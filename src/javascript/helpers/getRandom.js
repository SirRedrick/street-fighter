const getRandom = (min = 0, max = 1) => {
  return Math.random() * (max - min + 1);
};

export default getRandom;
